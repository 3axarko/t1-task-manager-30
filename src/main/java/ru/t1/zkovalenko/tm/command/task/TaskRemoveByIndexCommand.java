package ru.t1.zkovalenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove Task by index";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        getTaskService().removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
