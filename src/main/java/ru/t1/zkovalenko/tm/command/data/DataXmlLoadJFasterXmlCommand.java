package ru.t1.zkovalenko.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.Domain;
import ru.t1.zkovalenko.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadJFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    public static final String ARGUMENT = "-dlxf";

    @NotNull
    public static final String DESCRIPTION = "Load data xml FasterXML from file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML FASTERXML LOAD]");
        final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
        @NotNull final String xml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
