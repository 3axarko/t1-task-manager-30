package ru.t1.zkovalenko.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ConsoleColorConst {

    @NotNull
    public static final String RESET = "\u001B[0m";

    @NotNull
    public static final String BLACK = "\u001B[30m";
    @NotNull
    public static final String RED = "\u001B[31m";
    @NotNull
    public static final String GREEN = "\u001B[32m";
    @NotNull
    public static final String YELLOW = "\u001B[33m";
    @NotNull
    public static final String BLUE = "\u001B[34m";
    @NotNull
    public static final String PURPLE = "\u001B[35m";
    @NotNull
    public static final String CYAN = "\u001B[36m";
    @NotNull
    public static final String WHITE = "\u001B[37m";

}
